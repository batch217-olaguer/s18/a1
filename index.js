console.log("Hello World!!!!!!!!!!!!!!!!!!!!!");

var numberA = Math.floor(Math.random() * 11);
var numberB = Math.floor(Math.random() * 11);
;

// Addition
function add (n1,n2){
	console.log("Displayed sum of " +numberA+ " and " +numberB+ ":");
	console.log(n1 + n2);
}
var sum = add(numberA, numberB);

// Subtraction
function subtract (n1,n2){
	console.log("Displayed difference of " +numberA+ " and " +numberB+ ":");
	console.log(n1 - n2);
}
var difference = subtract(numberA, numberB);

// Multiplication
function multiply (n1,n2){
	console.log("Displayed product of " +numberA+ " and " +numberB+ ":");
	return n1 * n2;
}
var product = multiply(numberA, numberB);
// Invoking "product" variable;
console.log(product);

// Division
function divide (n1,n2){
	console.log("Displayed quotient of " +numberA+ " and " +numberB+ ":");	
	return n1 / n2;
}
var quotient = divide(numberA, numberB);
// Invoking "quotient" variable;
console.log(quotient);


/*==========================================================================*/

// Area of a Circle
var radius = Math.floor(Math.random() * 9);

function circle(r){
	console.log("The result of getting the area of a circle with " +radius+ " radius:");
	console.log(Math. PI * r**2);
	return Math. PI * r**2;
}
var circleArea = circle(radius);


/*===========================================================================*/
// Average of 4 numbers;
var num1 = Math.floor(Math.random() * 99);
var num2 = Math.floor(Math.random() * 99);
var num3 = Math.floor(Math.random() * 99);
var num4 = Math.floor(Math.random() * 99);

function averageOf4numbers (n1, n2, n3, n4){
	console.log("The average of " +num1+ ", " + num2+ ", " + num3+ ", " + "and " +num4 + ":");
	let avg4 = (num1+num2+num3+num4) / 4;
	console.log(avg4);
	return (n1+n2+n3+n4)/4;
}
var averageVar = averageOf4numbers(num1, num2, num3, num4);


/*===========================================================================*/
// Passing Score
var yourScore = Math.floor(Math.random() * 101);
var numberOfItems = 100;

function scorePercentage(s1){
	console.log("Is " +yourScore+ "/100 a passing score?");
	let isScorePercentage = yourScore/100 >= 0.75;
	return yourScore/100 >= 0.75;
}

var isPassingScore = scorePercentage(yourScore) >= 0.75;
console.log(isPassingScore);